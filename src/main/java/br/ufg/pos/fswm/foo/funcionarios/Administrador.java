package br.ufg.pos.fswm.foo.funcionarios;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/04/17.
 */
public class Administrador extends Funcionario {

    private double participacaoLucros;

    public Administrador(int matricula, String nome, double salarioBase, double participacaoLucros) {
        super(matricula, nome, salarioBase);
        this.participacaoLucros = participacaoLucros;
    }

    public double getParticipacaoLucros() {
        return participacaoLucros;
    }

    public void setParticipacaoLucros(double participacaoLucros) {
        this.participacaoLucros = participacaoLucros;
    }

    @Override
    public double calcularSalario() {
        return super.getSalarioBase() + participacaoLucros;
    }
}
