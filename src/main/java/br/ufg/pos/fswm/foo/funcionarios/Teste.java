package br.ufg.pos.fswm.foo.funcionarios;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/04/17.
 */
public class Teste {

    public static void main(String... args) {
        Horista f1 = new Horista(1, "fun1", 500, 50, 20);
        Horista f2 = new Horista(2, "fun2", 500, 45, 30);

        Administrador f3 = new Administrador(3, "fun3", 2000, 5000);
        Administrador f4 = new Administrador(4, "fun4", 2500, 7000);
        Administrador f5 = new Administrador(5, "fun5", 3000, 9000);
        Administrador f9 = new Administrador(9, "fun9", 3200, 11000);

        Comissionado f6 = new Comissionado(6, "fun6", 800, 15, 150.0);
        Comissionado f7 = new Comissionado(7, "fun7", 900, 12, 200.0);
        Comissionado f8 = new Comissionado(8, "fun8", 850, 20, 125.0);

        Gerente f10 = new Gerente(10, "fun10", 1500, 1000);

        Funcionario[] funcionarios = {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10};

        double folha = 0.0;

        for(Funcionario funcionario : funcionarios) {
            folha += funcionario.calcularSalario();
        }

        final String mensagem = String.format("Folha de pagamento totalizou R$ %.2f\n", folha);
        System.out.println(mensagem);


    }

}
