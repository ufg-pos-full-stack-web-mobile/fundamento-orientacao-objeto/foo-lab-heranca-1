package br.ufg.pos.fswm.foo.funcionarios;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/04/17.
 */
public class Comissionado extends Funcionario {

    private int itensVendidos;
    private double valorPorItem;

    public Comissionado(int matricula, String nome, double salarioBase, int itensVendidos, double valorPorItem) {
        super(matricula, nome, salarioBase);
        this.itensVendidos = itensVendidos;
        this.valorPorItem = valorPorItem;
    }

    public int getItensVendidos() {
        return itensVendidos;
    }

    public void setItensVendidos(int itensVendidos) {
        this.itensVendidos = itensVendidos;
    }

    public double getValorPorItem() {
        return valorPorItem;
    }

    public void setValorPorItem(double valorPorItem) {
        this.valorPorItem = valorPorItem;
    }

    @Override
    public double calcularSalario() {
        return super.getSalarioBase() + (valorPorItem * itensVendidos);
    }
}
