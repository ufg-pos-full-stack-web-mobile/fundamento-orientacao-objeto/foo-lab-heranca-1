package br.ufg.pos.fswm.foo.funcionarios;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/04/17.
 */
public class Gerente extends Funcionario {

    private double gratificacao;

    public Gerente(int matricula, String nome, double salarioBase, double gratificacao) {
        super(matricula, nome, salarioBase);
        this.gratificacao = gratificacao;
    }

    public double getGratificacao() {
        return gratificacao;
    }

    public void setGratificacao(double gratificacao) {
        this.gratificacao = gratificacao;
    }

    @Override
    public double calcularSalario() {
        return super.getSalarioBase() + gratificacao;
    }

}
