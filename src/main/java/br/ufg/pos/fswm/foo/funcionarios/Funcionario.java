package br.ufg.pos.fswm.foo.funcionarios;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/04/17.
 */
public abstract class Funcionario {

    private int matricula;
    private String nome;
    private double salarioBase;

    public Funcionario(int matricula, String nome, double salarioBase) {
        this.matricula = matricula;
        this.nome = nome;
        this.salarioBase = salarioBase;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public abstract double calcularSalario() ;
}
